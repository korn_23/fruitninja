#pragma once
#include <SFML/Graphics.hpp>
class Object
{
public:
	int x, y;

	Object(int x, int y) {
		this->x = x;
		this->y = y;
	}

	virtual void draw(sf::RenderWindow *w) = 0;
	virtual int click() = 0;
	//TODO: ������� ���������� click ��� ��������
};

