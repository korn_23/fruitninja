#pragma once
#include "Object.h"
class Perec :public Object {
private:
	sf::RectangleShape *rect;
	sf::Texture *tex;
public:
	Perec(int x ,int y ): Object(x,y) {
		rect = new sf::RectangleShape();
		tex = new sf::Texture();
		rect->setSize(sf::Vector2f(48, 48));
		rect->setPosition(sf::Vector2f(x, y));
		tex->loadFromFile("perec.png");
		rect->setTexture(tex);
	}
	void draw(sf::RenderWindow *w) {
		w->draw(*rect);
	}
	int click() {
		return -1;
	}
	~Perec() {
		delete rect;
		delete tex;
	}
};