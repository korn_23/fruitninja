#include<iostream>
#include <SFML/Graphics.hpp>
#include <time.h>
#include "Object.h"
#include "Orange.h"
#include "Apple.h"
#include "Perec.h"


int main()
{
	srand(time(NULL));
	sf::RenderWindow *window = new sf::RenderWindow(sf::VideoMode(800, 600), "Game");
	Object *objects[1000];
	for (int i = 0; i < 1000; i++)
	{
		objects[i] = nullptr;
	}

	int score = 0;

	bool endgame = false;

	sf::Clock clock;
	float t = 0;
	
	sf::Font *font = new sf::Font();
	font->loadFromFile("Bad Signal.otf");

	//Text 
	sf::Text *text = new sf::Text();
	text->setFont(*font);
	text->setCharacterSize(36);
	text->setFillColor(sf::Color::White);
	text->setPosition(sf::Vector2f(600, 550));
	text->setString("Score:"+std::to_string(score));
	
	sf::Text *you_lose = new sf::Text();
	you_lose->setFont(*font);
	you_lose->setCharacterSize(150);
	you_lose->setFillColor(sf::Color::White);
	you_lose->setPosition(sf::Vector2f(150, 150));
	you_lose->setString("You Lose!");

	sf::Text *retry = new sf::Text();
	retry->setFont(*font);
	retry->setCharacterSize(100);
	retry->setFillColor(sf::Color::White);
	retry->setPosition(sf::Vector2f(100, 400));
	retry->setString("Retry");

	sf::Text *exit = new sf::Text();
	exit->setFont(*font);
	exit->setCharacterSize(100);
	exit->setFillColor(sf::Color::White);
	exit->setPosition(sf::Vector2f(500, 400));
	exit->setString("Exit");
	
	

	int c = 0;  // ������� ��� ������� ��������

	while (window->isOpen()) {
		float dt = clock.restart().asSeconds();
		t += dt;

		sf::Event e;
		while (window->pollEvent(e)) {
			if (e.type == sf::Event::Closed) {
				window->close();
			}
			//���� �� ����� ����
			/*if (endgame) {
				if (e.type == sf::Event::MouseButtonReleased) {
					if (e.mouseButton.button == sf::Mouse::Button::Left)
					{
						if (e.mouseButton.x >= objects[i]->x&& e.mouseButton.y >= objects[i]->y &&
							e.mouseButton.x < objects[i]->x + 48 &&
							e.mouseButton.y < objects[i]->y + 48) {
					}
				}
			}*/
			//�������� �������� �� ������ 
			if (e.type == sf::Event::MouseButtonReleased) {
				if (e.mouseButton.button == sf::Mouse::Button::Left)
				{
					for (int i = 999; i > -1; i--) {
						if (objects[i] == nullptr)
							continue;

						if (e.mouseButton.x >= objects[i]->x&& e.mouseButton.y >= objects[i]->y &&
							e.mouseButton.x < objects[i]->x + 48 &&
							e.mouseButton.y < objects[i]->y + 48) {
							if (objects[i]->click() == -1)
							{
								endgame = true;
							}
							score++;
							text->setString("Score:" + std::to_string(score));
							delete objects[i];
							objects[i] = nullptr;
							break;
						}
					}
				}

			}
		}
		if (t > 1) {
			int x = rand() % 753;
			int y = rand() % 553;
			int a = rand() % 3;
			if (a == 0) {
				objects[c] = new Apple(x, y);
				c++;
				t = 0;
			}
			if (a == 1) {
				objects[c] = new Orange(x, y);
				c++;
				t = 0;
			}
			if (a == 2) {
				objects[c] = new Perec(x, y);
				c++; t = 0;
			}
		}

		window->clear(sf::Color::Black);
		//Draw here
		if (endgame) {
			window->draw(*you_lose);
			window->draw(*retry);
			window->draw(*exit);
		}
		else {
			for (int i = 0; i < 1000; i++) {
				if (objects[i] != nullptr) {
					objects[i]->draw(window);
				}
			}
			window->draw(*text);
			}
		window->display();
	}

		return 0;
}